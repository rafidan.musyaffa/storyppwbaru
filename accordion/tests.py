from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver
from .apps import AccordionConfig

class UnitTesting (TestCase):
    def test0_urlHomeExist(self):
        response = Client().get('/accord/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accord.html')

    def test_app_name(self):
            self.assertEqual(AccordionConfig.name, "accordion")