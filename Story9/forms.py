from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class loginForm(forms.Form):

    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': "Enter your username",
        'required': True,
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': "Enter your password",
        'required': True,
    }))


class registerForm(UserCreationForm):
    class Meta:
        model = User
        fields = [
            'username', 'email', 'password1','password2',
        ]
    
