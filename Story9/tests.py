from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from .forms import loginForm,registerForm
from .apps import Story9Config

class UnitTest(TestCase):
    def test_page_login(self):
        response = self.client.get('/Story9/login')
        self.assertEqual(response.status_code,200)
    
    def test_page_register(self):
        response = self.client.get('/Story9/register')
        self.assertEqual(response.status_code,200)

    def test_page_logout(self):
        response = self.client.get('/Story9/logout')
        self.assertEqual(response.status_code,302)

    def template_used_login(self):
        response = self.client.get(reverse('Story9:login'))
        self.assertTemplateUsed(response,'loginPage.html')

    def template_used_register(self):
        response = self.client.get(reverse('Story9:register'))
        self.assertTemplateUsed(response,'registerPage.html')
    
    def test_form_login_valid(self):
        form_login = loginForm(data={'username':"xxxx" , 'password':"abcdabcd"})
        self.assertTrue(form_login.is_valid())
        self.assertEqual(form_login.cleaned_data['username'],"xxxx")
        self.assertEqual(form_login.cleaned_data['password'],"abcdabcd")

    def test_form_register_valid(self):
        form_register = registerForm(data={
            'username':"xxxx" ,
            'email' : 'bbbbb@yahoo.com', 
            'password1':"abcdabcd",
            'password2':"abcdabcd"
        })
        self.assertTrue(form_register.is_valid())
        self.assertEqual(form_register.cleaned_data['username'],"xxxx")
        self.assertEqual(form_register.cleaned_data['email'],"bbbbb@yahoo.com")
        self.assertEqual(form_register.cleaned_data['password1'],"abcdabcd")
        self.assertEqual(form_register.cleaned_data['password2'],"abcdabcd")
        response = Client().get('/Story9/logout')
        self.assertRedirects(response,'/Story9/login')

    def test_logout_valid(self):
        self.client.logout
        response = Client().get('/Story9/logout')
        self.assertRedirects(response,'/Story9/login')

    def test_app_name(self):
        self.assertEqual(AccordionConfig.name, "Story9")
