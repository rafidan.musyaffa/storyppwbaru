from django.urls import path
from . import views
app_name = 'Story9'

urlpatterns = [
    path('/Story9/login', views.login_view, name='loginAwal'),
    path('login', views.login_view, name='login'),
    path('logout', views.logout_view, name='logout'),
    path('register', views.register_view, name='register')
]
