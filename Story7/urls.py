from django.urls import path
from . import views

app_name = 'Story7'

urlpatterns = [
    path('', views.home, name='home'),
    path('profile/', views.profile, name='profile'),

    path('hobby/', views.hobby, name='hobby'),
    path('education/', views.education, name='education'),

    path('sosmed', views.sosmed, name='sosmed'),
    path('contact/', views.contact, name='contact'),
    path('other/', views.other, name='other'),
    
]