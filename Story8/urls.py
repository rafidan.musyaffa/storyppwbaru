from django.urls import path
from . import views
app_name = 'Story8'

urlpatterns = [
    path('searchbooks', views.searchbooks, name='searchbooks'),
    path('searchbooks/data', views.data, name='books_data'),
]
