from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

# Create your views here.
def searchbooks(request):
    response = {}
    return render(request,'searchbooks.html',response)

def data(request):
    try:
        tes = request.GET['q']
    except:
        tes = "rafi"
    
    json_read = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + tes).json()

    return JsonResponse(json_read)
